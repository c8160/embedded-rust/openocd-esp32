openocd image, for running the Espressif fork of the OpenOCD debugger.

Packages the [openocd-esp32 project](https://github.com/espressif/openocd-esp32).

Volumes:
PWD: Mapped to /project in the container, used to exchange files with the host.
  Most basic usecase it to pass some default `openocd.cfg` to the container such
  that openocd automatically picks it up. This volume can be ommited if you
  don't need to share files with the host.

Documentation:
For the underlying openocd project, see https://openocd.org/
For this container, see https://gitlab.com/c8160/embedded-rust/openocd-esp32

Requirements:
Needs some USB device to run openocd against. The USB device must be accessible
by the user. This can be done by either `chmod`ing the respective entry under
/dev/bus/usb/ such that the calling user can access the device or running with
the necessary privileges. A udev rule can be added to automatically add user
access permissions to the USB device.

Configuration:
openocd is usually launched with some `openocd.cfg` in the working directory,
which it will automatically pick up and read. This can be ommited in favor of
specifying the necessary openocd files on the command line.

Note that in any case openocd must be configured to listen to '0.0.0.0' as bind
address, otherwise it will not be accessible from outside of the container.

