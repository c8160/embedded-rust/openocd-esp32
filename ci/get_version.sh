#!/usr/bin/env bash
#
# Get version string from container main executable.
# $1: The container image to run

VEROUT=$(podman run --rm -it "$1" --version)
VERLINE=$(echo $VEROUT | grep "^Open On-Chip" | tr -s ' ')
VERNUM=$(echo $VERLINE | cut -d' ' -f4 | cut -d'-' -f1 | tail -c+2)

if [[ -z "$VERNUM" ]]; then
    echo "FAILED to parse version number!" 1>&2
    echo "Output from '--version' was:" 1>&2
    echo "$VEROUT" 1>&2
    exit 1
fi

echo CONTAINER_EXE_VERSION=$VERNUM
echo CONTAINER_BUILD_DATE=$(date +%Y%m%d)
