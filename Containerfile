FROM docker.io/alpine:3.15

ENV NAME=openocd VERSION=0 ARCH=x86_64
LABEL   name="$NAME" \
        version="$VERSION" \
        architecture="$ARCH" \
        run="podman run --rm -it --security-opt label=disable -v '/dev:/dev' -p 3333:3333 IMAGE ..." \
        summary="Espressifs ESP32-compatible OpenOCD fork for embedded development." \
        maintainer="Andreas Hartmann <hartan@7x.de>" \
        url="https://gitlab.com/c8160/embedded-rust/openocd-esp32"

# Add documentation, configuration and helpers
COPY files/* /

WORKDIR /root
# Install packages
# This OpenOCD is linked against libc, so [we need gcompat][1]
# [1]: https://wiki.alpinelinux.org/wiki/Running_glibc_programs
RUN apk add --no-cache libusb gcompat
# Install espressif openocd from release
RUN wget -O openocd-esp32.tar.gz https://github.com/espressif/openocd-esp32/releases/download/v0.11.0-esp32-20220411/openocd-esp32-linux-amd64-0.11.0-esp32-20220411.tar.gz; \
    tar -xf openocd-esp32.tar.gz; \
    mv openocd-esp32/bin/openocd /usr/bin/openocd; \
    mv openocd-esp32/share/openocd /usr/share/openocd; \
    rm -rf openocd-esp32.tar.gz openocd-esp32

EXPOSE 3333
WORKDIR /project
VOLUME /project

ENTRYPOINT [ "/usr/bin/openocd" ]

