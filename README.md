# openocd-esp32

openocd-esp32 based on Alpine Linux.

> This image is tested for use with **podman** on **Linux** hosts with **x86_64** arch


## What is openocd-esp32?

OpenOCD-esp32 is a fork of [OpenOCD](https://openocd.org/) maintained and
developed by Espressif. It has several features missing in the upstream
version, most notably it includes configurations for various ESP32 boards.

See [the project website](https://github.com/espressif/openocd-esp32).

If you're looking for a regular OpenOCD container, refer to [my OpenOCD container]()


## How to use this image

### As a replacement for "regular" OpenOCD

```bash
$ podman run --rm -it --security-opt label=disable \
    -v /dev/bus/usb:/dev/bus/usb -p 3333:3333 --name openocd-esp32 \
    registry.gitlab.com/c8160/embedded-rust/openocd-esp32:latest
```

This will run OpenOCD without any arguments. You can then specifiy the OpenOCD
arguments at the end of the command above as additional inputs on the command
line.

We add the `/dev/bus/usb` directory from the host so the container can access
all attached USB devices. If you only want to attach a specific USB device,
read the **Additional Considerations** section below.

We also bind the containers port 3333 to the host. This is where GDB sessions
attach.

In order for you to access USB devices, their ownership permissions must be
overridden (i.e. `chmod`). This can be performed automatically via a
`udev`-rule whenever you plug the device in.

Additionally USB devices have special SELinux attributes and thus can't be
accessed directly by containers. There are multiple methods to make USB devices
accessible in containers:

1. Run the container with `--security-opt label=disable`: Disables SELinux
   labeling, least intrusive
2. Run the container as `--privileged`: Not recommended, it will expose all of
   `/sys` and `/dev` to the container which isn't needed.
3. Override SELinux settings on your machine via `setsebool -P
   container_use_devices 1`: Must be done on each host, not portable.


### With a custom *openocd.cfg*

Alternatively, in accordance with the [OpenOCD Config File
Guidelines](https://openocd.org/doc/html/Config-File-Guidelines.html), you may
create a `openocd.cfg` file in your current project and add it to the container
as a volume mount.

```bash
$ podman run --rm -it --security-opt label=disable -v /dev/bus/usb:/dev/bus/usb -p 3333:3333 --name openocd --volume "$PWD:/project" openocd -f openocd.cfg
```

In this case, make sure it includes a configuration line `bindto 0.0.0.0` or
`source /openocd_bind.cfg`.  Refer to the `openocd_bind.cfg` in this repository
for further information about this option.

In the containers root filesystem directory is also an example configuration
for debugging the ESP32 (`openocd_esp32.cfg`).


### With a containerized GDB

If your debugger of choice is run from a container as well, you can skip the
port binding via `-p 3333:3333` and run both containers from within the same
container network.

```bash
$ podman run --rm -it (OTHER OPTIONS) --network openocd_net --name openocd openocd
$ podman run --rm -it (OTHER OPTIONS) --network openocd_net gdb
```

In this case you must make sure that the usual

```
(gdb) target remote :3333
```

from within GDB now becomes

```
(gdb) target remote openocd:3333
```

where *openocd* **must** match the name of the OpenOCD container you specified
with the `--name` flag.



## Additional Considerations

In some circumstances it may be desirable to give the container access to only
specific USB devices and not all that are attached to the system. In this case
it is possible to mount only specific USB devices into the container. You will
have to determine the correct path for the USB device before you start. The one
you need is located under `/dev/bus/usb/<BUSID>/<DEVID>`.

You can get the `BUSID` and `DEVID` from the `lsusb` command. Alternatively you
may also use the `find_usb.sh` script available in this repository and the
container. The script requires a vendor (and optionally product) ID to know
which device to look out for. It can be called like so:

```bash
$ find_usb.sh 0483:
/dev/bus/usb/001/006
```

Or when using the container:

```bash
$ podman run --rm -it --security-opt label=disable -v /dev/bus/usb:/dev/bus/usb --entrypoint /find_usb.sh openocd 0483:
/dev/bus/usb/001/006
```


## Getting help

If you feel that something isn't working as expected, open an issue in the
Gitlab project for this container and describe what issue you are facing.

